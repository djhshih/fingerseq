#include <Rcpp.h>
using namespace Rcpp;

// Log sum of exponentials.
// This function does not handle NA or NaN values.
// [[Rcpp::export]]
double logsumexp(NumericVector xs) {
	size_t n = xs.size();
	double x_max = -INFINITY;
	double sum = 0.0;

	// find positive maximum value
	// (if we use the absolute maximum value and a very negative number is
	// chosen, then overflow can occur downstream)
	for (size_t i = 0; i < n; ++i) {
		double xi = xs[i];
		// if any value is \infinity then the logsumexp is \infinity
		if (xi == INFINITY) return INFINITY;
		if (xi == -INFINITY) continue;
		if (xi > x_max) x_max = xi;
	}
	if (x_max == -INFINITY) {
		// all values are -\infinity
		return -INFINITY;
	}

	// sum of exponentiation
	// subtract value with absolute maximum to avoid overflow or underflow
	// (overflow and underflow are still _possible_, but less likely)
	for (size_t i = 0; i < n; ++i) {
		double xi = xs[i];
		if (xi != -INFINITY) {
			// ignore \infinity because exp(-\infinity) = 0
			sum += exp(xi - x_max);
		}
	}

	return log1p(sum - 1) + x_max;
}
